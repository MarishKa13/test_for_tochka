# -*- coding: utf-8 -*-
import argparse
import views
from os import getenv

from parsing.database.models import Share
from config import app
from config import engine
from config import Base
from config import Session

from parsing.parsers.history_parser import parse_tickers_price_history
from parsing.parsers.insider_trades_parser import parse_insider_trades


def parse_txt_file_to_list_of_shades(filepath):
    with open(filepath, 'r') as file:
        tickers = [item.strip() for item in file.readlines()]

    return tickers


if __name__ == '__main__':
    cmd_parser = argparse.ArgumentParser()
    cmd_parser.add_argument('-f', dest='filepath', action='store', type=str, help='Path to file'
                                                                                  ' with tickers')
    cmd_parser.add_argument('-N', dest='ThreadNumber', action='store', type=int, help='Number of threads for parse'
                                                                                      ' Historical Stock')
    namespace = cmd_parser.parse_args()

    FILENAME = namespace.filepath
    THREADS = namespace.ThreadNumber

    session = Session()

    if getenv('UPDATE_DATA', False):
        Base.metadata.drop_all(bind=engine)

    Base.metadata.create_all(bind=engine)

    if len(session.query(Share).all()) == 0:
        shades_list = parse_txt_file_to_list_of_shades(FILENAME)

        if shades_list is not None:
            parse_tickers_price_history(THREADS, shades_list, Session)
            print(session.query(Share).all(), 'shares')
            parse_insider_trades(shades_list, Session)

    app.run()
