# -*- coding: utf-8 -*-


def convert_ticker(ticker_list):
    ticker_list_for_convert = []
    for item in ticker_list:
        ticker_as_dict = {
            'name': item.name
        }
        ticker_list_for_convert.append(ticker_as_dict)

    return ticker_list_for_convert


def convert_price(price_list):
    price_list_for_convert = []
    for item in price_list:
        price_as_dict = {
            'date': str(item.date),
            'open': item.open,
            'high': item.high,
            'low': item.low,
            'close': item.close
        }
        price_list_for_convert.append(price_as_dict)
        
    return price_list_for_convert


def convert_trade(trade_list):
    trade_list_for_convert = []
    for item in trade_list:
        trade_as_dict = {
            'co_owner': item.co_owner_id,
            'date': str(item.date),
            'transaction_type': item.transaction_type,
            'shares_traded': item.shares_traded,
            'last_price': item.last_price,
            'shares_held': item.shares_held
        }
        trade_list_for_convert.append(trade_as_dict)
        
    return trade_list_for_convert


def convert_analytics(prices):
    types = ['open', 'high', 'low', 'close']

    return dict(zip(types, prices))


def convert_minimal_ranges(ranges):
    range_dict = dict()
    for num, item in enumerate(ranges):
        range_dict[num] = str(item)
        
    return range_dict

