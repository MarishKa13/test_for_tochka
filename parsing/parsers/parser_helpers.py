# -*- coding: utf-8 -*-
import requests
from time import sleep
from datetime import date


def load_page(url):
    request_counter = 0
    while request_counter < 5:
        request = requests.get(url)
        if request.status_code == requests.codes.ok:
            return request.text
        else:
            sleep(3)

        request_counter += 1

    return None


def string_to_float(string_to_convert):
    try:
        return float(string_to_convert)
    except ValueError:
        if string_to_convert != '':
            return float(''.join(string_to_convert.split(',')))
        else:
            return None


def string_to_int(string_to_convert):
    try:
        return int(''.join(string_to_convert.split(',')))
    except ValueError:
        return None


def string_to_date(string_to_convert):
    try:
        list_from_date_string = string_to_convert.split('/')
        return date(int(list_from_date_string[2]), int(list_from_date_string[0]),
                    int(list_from_date_string[1]))
    except (IndexError, ValueError):
        return date.today()
