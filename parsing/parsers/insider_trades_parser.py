# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup

from ..database.models import CoOwner, CompanyAndCoOwner, Trade, Share
import parsing.parsers.parser_helpers as helpers


def find_number_of_pages(html_text):
    soup = BeautifulSoup(html_text, 'html.parser')
    try:
        pages = soup.find('ul', {'class': 'pager'}).find_all('li')
        last_page_number = int(pages[-3].text.strip())
        if last_page_number > 10:
            last_page_number = 10

        return last_page_number

    except AttributeError:

        return None


def get_info_about_trades(html_text):
    soup = BeautifulSoup(html_text, 'html.parser')
    try:
        table_rows = soup.find('table', {'class': 'certain-width'}).find_all('tr')
        table_cells_by_rows = []
        for rows in table_rows:
            cells = rows.find_all('td')
            table_cells_by_rows.append(cells)

        return table_cells_by_rows[1:]

    except AttributeError:

        return None


def fill_database(ticker_name, info_from_html, session):
    insider_trades_session = session()
    if info_from_html is None:

        return

    for rows in info_from_html:
        co_owner_name = rows[0].find('a').text
        if (len(session.query(CoOwner).filter(CoOwner.name == co_owner_name).all())) == 0:
            co_owner = CoOwner(co_owner_name, rows[1].text, rows[4].text)
            insider_trades_session.add(co_owner)
            insider_trades_session.commit()
            co_owner_id = co_owner.id
        else:
            co_owner_from_bd = session.query(CoOwner).filter(CoOwner.name == co_owner_name).first()
            co_owner_id = co_owner_from_bd.id

        share = insider_trades_session.query(Share).filter(Share.name == ticker_name.upper()).first()
        company_co_owner = CompanyAndCoOwner(share.company_id, co_owner_id)
        insider_trades_session.add(company_co_owner)

        trade_date = helpers.string_to_date(rows[2].text.strip())
        last_price = helpers.string_to_float(rows[6].text.strip())
        shares_traded = helpers.string_to_int(rows[5].text.strip())
        shares_held = helpers.string_to_int(rows[7].text.strip())

        trade = Trade(trade_date, co_owner_id, rows[3].text, shares_traded, last_price, shares_held)
        insider_trades_session.add(trade)

        insider_trades_session.commit()


def parse_insider_trades(list_of_tickers, session):
    url = 'http://www.nasdaq.com/symbol/{}/insider-trades{}'
    for ticker in list_of_tickers:
        last_page = find_number_of_pages(helpers.load_page(url.format(ticker.lower(), '')))

        if last_page is None:
            continue

        trades_info = []

        for number in range(1, last_page + 1):
            html_text = helpers.load_page(url.format(ticker.lower(), '?page={}'.format(str(number))))
            trades_info.extend(get_info_about_trades(html_text))

        fill_database(ticker, trades_info, session)
