# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from threading import Thread

from ..database.models import Share, Price, Company
import parsing.parsers.parser_helpers as helpers


class ThreadForParseTickersHistory(Thread):
    def __init__(self, ticker_name, session):
        Thread.__init__(self)

        self.daemon = True
        self.name = 'Thread - {}'.format(ticker_name)
        self.session = session
        self.ticker_name = ticker_name

    def run(self):
        html_text = helpers.load_page('http://www.nasdaq.com/symbol/{}/historical'.format(self.ticker_name.lower()))
        soup = BeautifulSoup(html_text, 'html.parser')
        price_info = find_price_info(soup)
        company_name = find_company_name(soup)
        fill_database(self.ticker_name, price_info, company_name, self.session)


def find_price_info(soup):
    try:
        div_with_table = soup.find('div', {'id': 'quotes_content_left_pnlAJAX'})
        table = div_with_table.find('table').find('tbody')
        table_strings = table.find_all('tr')

        table_cells_by_rows = []
        for string in table_strings:
            table_cells_by_rows.append(string.find_all('td'))

        return table_strings

    except AttributeError:

        return None


def find_company_name(soup):
    company_name = soup.find('title').text.split('(')[0]

    return company_name


def fill_database(ticker_name, price_info, company_name, session):
    if price_info is None:
        return

    if len(company_name) == 1:
        company_name = '{}_company'.format(ticker_name)

    thread_session = session()
    if len(thread_session.query(Company).filter(Company.name == company_name).all()) == 0:
        company = Company(company_name)
        thread_session.add(company)
        thread_session.commit()
    else:
        company = thread_session.query(Company).filter(Company.name == company_name).first()

    share = Share(ticker_name.upper(), company.id)
    thread_session.add(share)
    thread_session.commit()

    for row in price_info:
        table_cells = row.find_all('td')

        price_date = helpers.string_to_date(table_cells[0].text.strip())

        open_price = helpers.string_to_float(table_cells[1].text.strip())
        high_price = helpers.string_to_float(table_cells[2].text.strip())
        low_price = helpers.string_to_float(table_cells[3].text.strip())
        close_price = helpers.string_to_float(table_cells[4].text.strip())

        volume = helpers.string_to_int(table_cells[5].text.strip())

        if not(open_price is None and high_price is None and low_price is None and close_price is None):
            price = Price(share_id=share.id, date=price_date, open_price=open_price, high_price=high_price,
                          low_price=low_price, close_price=close_price, volume=volume)
            thread_session.add(price)

    thread_session.commit()


def parse_tickers_price_history(N, list_of_tickers, session):
    iteration = len(list_of_tickers) // N
    if len(list_of_tickers) % N != 0:
        iteration += 1

    repeat = 0
    while iteration > repeat:
        threads = []
        list_part = list_of_tickers[N*repeat:N*(repeat+1)]

        for item in list_part:
            thread = ThreadForParseTickersHistory(item, session)
            threads.append(thread)

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

        repeat += 1
