# -*- coding: utf-8 -*-
from sqlalchemy import Column
from sqlalchemy.orm import relationship
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import ForeignKey
from sqlalchemy import Float
from sqlalchemy import Date
from config import Base


class CompanyAndCoOwner(Base):
    __tablename__ = 'Company_CoOwner'
    id = Column(Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey('Company.id'), nullable=False)
    co_owner_id = Column(Integer, ForeignKey('CoOwner.id'), nullable=False)

    def __init__(self, company_id, co_owner_id):
        self.company_id = company_id
        self.co_owner_id = co_owner_id

    def __repr__(self):
        return '<Company_CoOwner(company_id={}, co_owner_id={})>'.format(self.company_id, self.co_owner_id)


class Company(Base):
    __tablename__ = 'Company'
    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)

    share = relationship('Share', backref='Company')

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Company(name={})>'.format(self.name)


class CoOwner(Base):
    __tablename__ = 'CoOwner'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)
    relation = Column(String(20))
    owner_type = Column(String(20))

    def __init__(self, name, relation, owner_type):
        self.name = name
        self.relation = relation
        self.owner_type = owner_type

    def __repr__(self):
        return '<CoOwner(name={}, relation={}, owner_type={})>'.format(self.name, self.relation, self.owner_type)


class Trade(Base):
    __tablename__ = 'Trade'
    id = Column(Integer, primary_key=True)
    date = Column(Date)
    co_owner_id = Column(Integer, ForeignKey('CoOwner.id'), nullable=False)
    transaction_type = Column(String(50))
    shares_traded = Column(Integer)
    last_price = Column(Float)
    shares_held = Column(Integer)

    def __init__(self, date, co_owner_id, transaction_type, shares_traded, last_price, shares_held):
        self.date = date
        self.co_owner_id = co_owner_id
        self.transaction_type = transaction_type
        self.shares_traded = shares_traded
        self.last_price = last_price
        self.shares_held = shares_held

    def __repr__(self):
        return '<Trade(date={}, co_owner_id={}, transaction_type={}, shares_traded={},' \
               ' last_price={}, shares_held={})>'.format(self.date, self.co_owner_id, self.shares_traded,
                                                         self.transaction_type, self.last_price, self. shares_held)


class Share(Base):
    __tablename__ = 'Share'
    id = Column(Integer, primary_key=True)
    name = Column(String(10))
    company_id = Column(Integer, ForeignKey('Company.id'), nullable=False)

    price = relationship('Price', backref='Share')

    def __init__(self, name, company_id):
        self.name = name
        self.company_id = company_id

    def __repr__(self):
        return '<Share({}, {})>'.format(self.name, self.company_id)


class Price(Base):
    __tablename__ = 'Price'
    id = Column(Integer, primary_key=True)
    share_id = Column(Integer, ForeignKey('Share.id'), nullable=False)
    date = Column(Date)
    open = Column(Float)
    high = Column(Float)
    low = Column(Float)
    close = Column(Float)
    volume = Column(Integer)

    def __init__(self, share_id, date, open_price, high_price, low_price, close_price, volume):
        self.share_id = share_id
        self.date = date
        self.open = open_price
        self.high = high_price
        self.low = low_price
        self.close = close_price
        self.volume = volume

    def __repr__(self):
        return '<Price(share_id={}, date={}, open={}, high={},' \
               ' low={}, close={}, volume={})>'.format(self.share_id, self.date, self.open, self.high, self.low,
                                                       self.close, self.volume)
