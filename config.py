from flask import Flask
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session


Base = declarative_base()

app = Flask(__name__)

engine = create_engine('postgresql://postgres:SecretKey@localhost:5432/test_for_tochka', echo=True)

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
