# -*- coding: utf-8  -*-
from parsing.database.models import Share
from parsing.database.models import Trade
from parsing.database.models import CoOwner
from parsing.database.models import CompanyAndCoOwner
from parsing.database.models import Company
from parsing.database.models import Price
from config import app
from config import Session
import convert_to_json

from flask import render_template
from flask import request
from flask import jsonify
from datetime import date

SESSION = Session()
PREFIX = '/api'


@app.route('/', methods=['GET'])
@app.route(PREFIX+'/', methods=['GET'])
def index_view():
    tickers = SESSION.query(Share).all()
    if PREFIX in request.full_path:
        json_data = convert_to_json.convert_ticker(tickers)
        return jsonify(json_data)
    else:
        return render_template('index.html', tickers=tickers)


@app.route('/<ticker_name>', methods=['GET'])
@app.route(PREFIX+'/<ticker_name>', methods=['GET'])
def ticker_history_view(ticker_name):
    ticker = SESSION.query(Share).filter(Share.name == ticker_name).first()

    if ticker is None:
        return render_template('404.html')

    prices = SESSION.query(Price).filter(Price.share_id == ticker.id).all()
    if PREFIX in request.full_path:
        json_data = convert_to_json.convert_price(prices)
        return jsonify(json_data)

    return render_template('share.html', prices=prices, ticker=ticker_name)


def co_owner_name_from_id(id_number):
    co_owner = SESSION.query(CoOwner).filter(CoOwner.id == id_number).first()
    return co_owner.name


@app.route('/<ticker_name>/insider', methods=['GET'])
@app.route(PREFIX+'/<ticker_name>/insider', methods=['GET'])
def insider_trades_view(ticker_name):
    ticker = SESSION.query(Share).filter(Share.name == ticker_name).first()
    company = SESSION.query(Company).filter(Company.id == ticker.company_id).first()

    if ticker is None or company is None:
        return render_template('404.html')

    query = SESSION.query(CompanyAndCoOwner).filter(CompanyAndCoOwner.company_id == ticker.company_id).all()
    co_owners_id = list(set([item.co_owner_id for item in query]))
    trades = [item for item in SESSION.query(Trade).all() if item.co_owner_id in co_owners_id]

    if PREFIX in request.full_path:
        json_data = convert_to_json.convert_trade(trades)
        return jsonify(json_data)

    return render_template('insider_trades.html', find_name=co_owner_name_from_id, trades=trades,
                           ticker=ticker_name, company_name=company.name)


@app.route('/<ticker_name>/insider/<name>', methods=['GET'])
@app.route(PREFIX+'/<ticker_name>/insider/<name>', methods=['GET'])
def insider_trade_by_name_view(ticker_name, name):
    ticker = SESSION.query(Share).filter(Share.name == ticker_name).first()
    company = SESSION.query(Company).filter(Company.id == ticker.company_id).first()
    co_owner = SESSION.query(CoOwner).filter(CoOwner.name == name).first()

    if ticker is None or company is None or co_owner is None:
        return render_template('404.html')

    trades = SESSION.query(Trade).filter(Trade.co_owner_id == co_owner.id).all()
    if PREFIX in request.full_path:
        json_data = convert_to_json.convert_trade(trades)
        return jsonify(json_data)

    return render_template('trades_by_name.html', company_name=company.name, insider=co_owner, trades=trades,
                           ticker=ticker_name)


@app.route('/<ticker_name>/analytics', methods=['GET'])
@app.route(PREFIX+'/<ticker_name>/analytics', methods=['GET'])
def ticker_analytics_view(ticker_name):
    prices = None
    date_from = request.args.get('date_from')
    date_to = request.args.get('date_to')

    if date_from is None or date_to is None:
        return render_template('share_analytics.html', ticker=ticker_name, prices=prices, date_from=date_from,
                               date_to=date_to)

    ticker = SESSION.query(Share).filter(Share.name == ticker_name).first()
    price_for_current_ticker = SESSION.query(Price).filter(Price.share_id == ticker.id)
    price_start = price_for_current_ticker.filter(Price.date == string_to_date(date_from)).first()
    price_end = price_for_current_ticker.filter(Price.date == string_to_date(date_to)).first()

    try:
        price_close_delta = round(price_start.close - price_end.close, 6)
        price_low_delta = round(price_start.low - price_end.low, 6)
        price_high_delta = round(price_start.high - price_end.high, 6)
        price_open_delta = round(price_start.open - price_end.open, 6)
        prices = dict(open=price_open_delta, high=price_high_delta, low=price_low_delta, close=price_close_delta)

    except AttributeError:
        pass

    if PREFIX in request.full_path:
        json_data = convert_to_json.convert_analytics(prices)
        return jsonify(json_data)

    return render_template('share_analytics.html', ticker=ticker_name, prices=prices, date_from=date_from,
                           date_to=date_to)


def string_to_date(string_to_convert):
    split_string = string_to_convert.split('-')

    return date(int(split_string[0]), int(split_string[1]), int(split_string[2]))


@app.route('/<ticker_name>/delta', methods=['GET'])
@app.route(PREFIX+'/<ticker_name>/delta', methods=['GET'])
def minimal_range_for_delta_view(ticker_name):
    ticker = SESSION.query(Share).filter(Share.name == ticker_name).first()

    if ticker is None:
        return render_template('404.html')

    price_for_current_ticker = SESSION.query(Price).filter(Price.share_id == ticker.id).all()
    ranges = []

    if request.args.get('value') is None or request.args.get('price_type') is None:
        return render_template('share_minimal_period_for_delta.html', ticker=ticker_name, ranges=ranges)

    N = int(request.args.get('value'))
    price_type = request.args.get('price_type')

    price_for_current_ticker.reverse()

    delta = 0
    number_of_start_date = 0
    ranges = []

    for num, item in enumerate(price_for_current_ticker):
        current_price = type_to_attribute_item(price_type, item)

        if num == 0:
            previous_price = current_price
        else:
            previous_price = type_to_attribute_item(price_type, price_for_current_ticker[num - 1])

        delta += (current_price - previous_price)
        if delta >= N > 0 or delta <= N < 0:
            ranges.append([price_for_current_ticker[number_of_start_date].date,
                           price_for_current_ticker[num].date])
            number_of_start_date = num
            delta = 0

    if PREFIX in request.full_path:
        json_data = convert_to_json.convert_minimal_ranges(ranges)
        return jsonify(json_data)

    return render_template('share_minimal_period_for_delta.html', ticker=ticker_name, ranges=ranges)


def type_to_attribute_item(price_type, item):
    attribute_dict = {price_type == 'open': item.open,
                      price_type == 'high': item.high,
                      price_type == 'low': item.low,
                      price_type == 'close': item.close}

    return attribute_dict[True]

